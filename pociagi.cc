/* JNP 1 - Zadanie zaliczeniowe nr 1
 * Rok akademicki 2013/2014, semestr zimowy
 * Wykonali: Dawid Łazarczyk (dl337614) i Jakub Radzicki (jr337655)
 * dodać do opcji kompilacji -lboost_regex
*/
#include <iostream>
#include <sstream>
#include <climits>
#include <vector>
#include <utility>
#include <algorithm>
#include <numeric>
#include <boost/regex.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>

using namespace std;

typedef int delay;
typedef short arrival;
typedef vector<pair<arrival, delay> > trains;

const int MAX_DELAY = INT_MAX;
const int MIN_IN_HOUR = 60; // a number of minutes in an hour
const int HOURS_IN_DAY = 24;
const int MIN_IN_DAY = MIN_IN_HOUR * HOURS_IN_DAY;
const int MAX_LENGTH = 2000;

const string TRAIN_INFO_PATTERN =
    "^\\s*(?<train_id>\\d{3,9})"
    "\\s+(?<day>\\d?\\d)\\.(?<month>\\d?\\d)\\.(?<year>\\d{4})"
    "\\s+(?<hour>\\d?\\d)\\.(?<minute>\\d\\d)"
    "(\\s+(?<delay>\\d{1,9}))?\\s*$";

const string QUERY_PATTERN =
    "^\\s*(?<type>[LMS])\\s+(?<beg_hour>\\d?\\d)\\.(?<beg_min>\\d\\d)"
    "\\s+(?<end_hour>\\d?\\d)\\.(?<end_min>\\d\\d)\\s*$";

int ParseStringToInt(const string &str) {
    istringstream i(str);
    int nr;
    i >> nr;
    return nr;
}

bool IsDateValid(int day, int month, int year) {
    try {
        boost::gregorian::date d(year, month, day);
        return true;
    }
    catch (out_of_range E) {
        return false;
    }
}

bool IsTimeValid(int minute, int hour) {
    return minute >= 0 && minute < MIN_IN_HOUR &&
        hour >= 0 && hour < HOURS_IN_DAY;
}

int GetMinutes(int hour, int minute) {
    return MIN_IN_HOUR * hour + minute;
}

void PrintErrorLine(int counter, const string &line) {
    cerr << "Error " << counter << ": " << line << endl;
}

// If it's possible it updates data structure with info from 'line'.
// The result: true iff the line was correct line with the information about
// train.
bool ProcessTrainInformation(trains &trains_container, const string &line) {
    static const boost::regex expr(TRAIN_INFO_PATTERN);
    boost::match_results<string::const_iterator> result;

    if (boost::regex_match(line, result, expr)) {
        int minute, hour, day, month, year, delay;
        minute = ParseStringToInt(result["minute"]);
        hour = ParseStringToInt(result["hour"]);
        day = ParseStringToInt(result["day"]);
        month = ParseStringToInt(result["month"]);
        year = ParseStringToInt(result["year"]);

        if (result["delay"].matched)
            delay = ParseStringToInt(result["delay"]);
        else
            delay = 0;

        if (IsDateValid(day, month, year) && IsTimeValid(minute, hour) &&
            delay <= MAX_DELAY) {

            arrival min_of_day = (GetMinutes(hour, minute) + delay)
                % MIN_IN_DAY; // which minute of day is it
            trains_container.push_back(make_pair(min_of_day, delay));
            return true;
        }
    }
    return false;
}

// Helping class for adding delays of trains
struct AddingClass {
    long long operator()(long long accumulator, const pair<int, int> &el) {
        return accumulator + (long long) el.second;
    }
};

// Comparing class for train's real time of arrival.
// We don't want the default behaviour because we should
// ignore the delay during checking whether a train arrived in a
// given interval.
struct TimeComp {
    bool operator()(const pair<arrival, delay> &a, const pair<arrival, delay> &b) {
        return a.first < b.first;
    }
};

// Comparing class for train's delay
struct DelayComp {
    bool operator()(const pair<arrival, delay> &a, const pair<arrival, delay> &b) {
        return a.second < b.second;
    }
};

void AnswerQuery(char type, trains &train_container,
                 arrival beg_time, arrival end_time) {
    // Iterators responsible for finding beginning and one item after
    // the end of subsequence of trains from the given interval,
    // responsively.
    trains::iterator beg, end;
    beg = lower_bound(train_container.begin(), train_container.end(),
                      make_pair(beg_time, 0), TimeComp());
    end = upper_bound(train_container.begin(), train_container.end(),
                      make_pair(end_time, 0), TimeComp());
    switch (type) {
        case 'L': {
            cout << end - beg << endl;
            break;
        }
        case 'M': {
            if (beg == end)
                cout << 0 << endl;
            else
                cout << max_element(beg, end, DelayComp())->second << endl;
            break;
        }
        case 'S': {
            cout << accumulate(beg, end, 0LL, AddingClass()) << endl;
            break;
        }
    }
}

// Tries to get query arguments.
// Returns true iff the line is a correct query line.
// It writes the parsed arguments to variables type and interval.
bool ParseQuery(const string &line, char &type,
                pair<arrival, arrival> &interval) {
    static const boost::regex expr(QUERY_PATTERN);
    boost::match_results<string::const_iterator> result;

    if (boost::regex_match(line, result, expr)) {
        int beg_min = ParseStringToInt(result["beg_min"]),
            beg_hour = ParseStringToInt(result["beg_hour"]),
            end_min = ParseStringToInt(result["end_min"]),
            end_hour = ParseStringToInt(result["end_hour"]);
        char qtype = ((string)result["type"])[0];
        arrival beg_time = GetMinutes(beg_hour, beg_min),
                end_time = GetMinutes(end_hour, end_min);

        if (IsTimeValid(beg_min, beg_hour) &&
            IsTimeValid(end_min, end_hour) &&
            beg_time <= end_time) {

            type = qtype;
            interval = make_pair(beg_time, end_time);
            return true;
        }
    }
    return false;
}

// True, iff the query is correct. If it is, the function answers it.
// It sorts the container, if needed.
bool ProcessQuery(trains &train_container, const string &line,
                       bool sort_it_first = false) {
    pair<arrival, arrival> interval;
    char type;

    if (ParseQuery(line, type, interval)) {
        if (sort_it_first)
            sort(train_container.begin(), train_container.end(), TimeComp());
        AnswerQuery(type, train_container, interval.first, interval.second);
        return true;
    }
    return false;
}

int main() {
    ios_base::sync_with_stdio(0);
    string line;
    trains train_container;
    bool query_appeared = false;
    int counter = 1;

    while (getline(cin, line)) {
        if (query_appeared) {
            if (!ProcessQuery(train_container, line))
                PrintErrorLine(counter, line);
        }
        else {
            if (!ProcessTrainInformation(train_container, line)) {
                // try to process query and sort container first
                // if the query is correct
                if (ProcessQuery(train_container, line, true))
                    query_appeared = true;
                else
                    PrintErrorLine(counter, line);
            }
        }
        counter++;
    }
    return 0;
}
